defmodule BullsAndCows do
  def score_guess(secret, guess) do
    Enum.join(secret, guess)
    |> Enum.reduce({0,0}, fn {s,g},{bulls,cows} ->
      cond do
        g == s      -> {bulls + 1, cows}
        g in secret -> {bulls, cows + 1}
        true        -> {bulls, cows}
      end
    end)

    {bulls, cows} = score_guess(secret, guess)
    IO.puts "#{bulls} Bulls, #{cows} Cows"
  end
end
